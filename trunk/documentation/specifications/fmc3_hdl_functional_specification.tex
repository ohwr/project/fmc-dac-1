\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{ae,aecompl}
\usepackage{parskip} % no paragraph indentation
\usepackage[pdftex]{color,graphicx}
\usepackage{hyperref}
\usepackage{verbatim}
\usepackage{vhistory}
 

% Uncomment the following two lines to check syntax only (no .dvi output produced, so it's faster!)
%\usepackage{syntonly}
%\syntaxonly

%\pagestyle{headings}


% Define the title
\title{FMC3 HDL Functional Specification}
\author{Matthieu Cattin}

\begin{document}

%.jpg can only be seen in pdf file (not in dvi)!
\begin{figure}[t]
	\includegraphics[scale=0.5]{cern_logo.pdf}
	\label{fig:cern_logo}
\end{figure}
%Generate the title
\maketitle

% Insert a table of content
%\tableofcontents

\begin{abstract}
FMC3 is a 4-channel 16-bit 10Ms/s DAC board in FMC (FPGA Mezzanine Card~\cite{fmcstd}) form-factor.
This document describes the HDL related to the FMC3 card. For more information on hardware related specification, see \textit{FMC3 functional specification}.
\end{abstract}


\newpage

\begin{versionhistory}
\vhEntry{1.0}{10-20-2009}{mcattin}{First draft of functional specifications.}
\vhEntry{1.1}{10-21-2009}{mcattin}{Minor corrections after internal cern review.}
\vhEntry{1.2}{10-22-2009}{mcattin}{Remove "abort" trigger input from functional specifications.}
\vhEntry{1.3}{10-26-2009}{mcattin}{Add a revision history.}
\end{versionhistory}

This document may have been updated since you printed it. In case of doubts, please look in
\href{http://www.ohwr.org/svn/FmcDac1/trunk/documentation/specifications/fmc3_hdl_functional_specification.pdf}{www.ohwr.org} for the latest version and compare its History of versions with yours.

\newpage 

\begin{comment}
\textcolor{red}{
\section{Background}
In the context of the PS complex renovation project (ACCOR~\cite{accorweb}), it has been decided to design new boards in FMC form-factor.
The current slow waveform generator (GFAS, CVORB) is only 250kS/s and does not cover new needs in the mid-range of sampling frequencies. So, to avoid installing fast waveform generators for those cases, it has been decided to design a faster "slow" waveform generator.
Previous slow waveform generators (GFAS and CVORB) have digital serial outputs only, so an additional card in 3U format including a DAC is needed in that case.
}
\end{comment}

\section{Features}
\begin{itemize}
	\item 4 single-ended analog output channels
	\item Output voltage range : +/-10V (50 ohms)
	\item	16-bit DACs
	\item Up to 10MS/s
	\item 1 external clock input (single-ended)
	\item 1 start input (TTL)
	\item 1 pause input (TTL)
	\item Each channel can store up to 32 waveform.
	\item Memory/channel is determined by FMC carrier~\cite{vmefmccarier}~\cite{pciefmccarier}.
	\item Analog outputs auto-calibration.
	\item $I^2C$ EEPROM to store IPMI information and DACs calibration factors
	\item 2 modes (vectors and points)
\end{itemize}


\section{General description}
An FMC3 is a mezzanine card in FMC format, containing four identical analog output channels. Each channel has a 10MS/s 16-bit DAC with an output range of +/-10V. In addition, the card has two trigger inputs (start and pause) and one external clock input. Those inputs are common to the four analog channels.


\section{Modes}
\subsection{Vector mode}
The vector mode principle works as follows: a waveform stored in memory contains only a set of vectors (intermediate values to be sent to the DAC are computed on-board). A vector is made of 3 parts, one for the amplitude and two for the time :
\begin{itemize}
	\item amplitude value (next amplitude to reach)
	\item step size (number of sampling clock period for a step)
	\item number of steps
\end{itemize}

The first 3 values of a waveform are different from the above vector type.
\begin{itemize}
	\item total number of vectors
	\item repeat number (0=infinite)
	\item first amplitude value
\end{itemize}

Memory can store up to 32 different waveforms per channel. Each waveform will have a fixed maximum length depending on the FMC carrier card capacity~\cite{vmefmccarier}~\cite{pciefmccarier}. The carrier should provide enough memory for at least 8000 vectors per waveform.

Each channel has a register allowing users to select the waveform to be played on next start pulse.

One mask register per channel allows to selectively enable the 32 waveforms. If a waveform is selected but disabled in the mask, the start trigger is ignored.

In vector mode, the sampling frequency is fixed (10MS/s). 

\begin{comment}
But it can also be selectable between a certain number of predefined values. This has to be decided with the higher software layer developers.
\end{comment}

All digital values sent to the DACs are computed inside the FMC carrier from the vectors in memory.


\subsection{Point mode}
In addition to the vector mode, a point mode will also be implemented. In this case, all  values to be sent to the DAC will be stored in memory.

A table describes a set of waveforms. Each waveform is defined by:

\begin{itemize}
	\item An address pointer.
	\item The waveform size.
	\item A repetition number.
	\item The next waveform to play (if any).
\end{itemize}

The maximum number of waveform is 20 (to be confirmed). Waveforms can be chained using the "Next waveform to play" field.

In point mode, sampling clock can be internal or external. In case of an internal clock, the frequency can be selected over a wide range. It will depend on the carrier clock distribution scheme~\cite{vmefmccarier}~\cite{pciefmccarier}.


\section{Triggers}
The FMC3 has two trigger inputs. A start and a pause input. They can be used in many different ways.

The first and simplest case uses only the start input. The selected waveform is generated when a start pulse arrives. Generation continues until the end of the waveform.

The "pause" trigger input is used to suspend the waveform generation. After a pause pulse, a new start pulse is needed in order to continue waveform generation.

Moreover, in vector mode, it is possible to program a pause inside a waveform. This is done by means of a special vector called "internal pause". To continue the waveform generation a start pulse is needed, as for the pause trigger input.

For test purposes, all the trigger inputs are also emulated in software by writing in a register.

Trigger inputs polarity is selectable via a register (TTL or $\overline{TTL}$). 


\section{Test}
Some test features will be implemented to easily test hardware during development and installation.
The two test features are:
\begin{itemize}
	\item Test waveform generation (saw-tooth).
	\item Direct digital value to DAC.
\end{itemize}


\bibliographystyle{plain}
\bibliography{fmc3_references}

\end{document}
