\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{ae,aecompl}
\usepackage{parskip} % no paragraph indentation
\usepackage[pdftex]{color,graphicx}
\usepackage{hyperref}
\usepackage{verbatim}
\usepackage{vhistory}


\pdfminorversion=5

% Uncomment the following two lines to check syntax only (no .dvi output produced, so it's faster!)
%\usepackage{syntonly}
%\syntaxonly

%\pagestyle{headings}


% Define the title
\title{FMC3 Functional Specification}
\author{Matthieu Cattin}

\begin{document}

%.jpg can only be seen in pdf file (not in dvi)!
\begin{figure}[t]
	\includegraphics[scale=0.5]{cern_logo.pdf}
	\label{fig:cern_logo}
\end{figure}
%Generate the title
\maketitle

% Insert a table of content
%\tableofcontents

\begin{abstract}
FMC3 is a 4-channel 16-bit 10Ms/s DAC card in FMC (FPGA Mezzanine Card~\cite{fmcstd}) form-factor.
This document describes the hardware only. Another functional specification is dedicated to the HDL related to the FMC3 card (see \textit{FMC3 HDL functional specification}).
\end{abstract}


\newpage

\begin{versionhistory}
\vhEntry{1.0}{10-20-2009}{mcattin}{First draft of functional specifications.}
\vhEntry{1.1}{10-21-2009}{mcattin}{Minor corrections after internal cern review.}
\vhEntry{1.2}{10-22-2009}{mcattin}{Remove "abort" trigger input from functional specifications.}
\vhEntry{1.3}{10-26-2009}{mcattin}{Add a revision history.}
\end{versionhistory}

This document may have been updated since you printed it. In case of doubts, please look in
\href{http://www.ohwr.org/svn/FmcDac1/trunk/documentation/specifications/fmc3_functional_specification.pdf}{www.ohwr.org} for the latest version and compare its History of versions with yours.

\newpage 

\begin{comment}
\textcolor{red}{
\section{Background}
In the context of the PS complex renovation project (ACCOR~\cite{accorweb}), it has been decided to design new boards in FMC form-factor.
The current slow waveform generator (GFAS, CVORB) is only 250kS/s and does not cover new needs in the mid-range of sampling frequencies. So, to avoid installing fast waveform generators for those cases, it has been decided to design a faster "slow" waveform generator.
Previous slow waveform generators (GFAS and CVORB) have digital serial outputs only, so an additional card in 3U format including a DAC is needed in that case.
}
\end{comment}

\section{Features}
\begin{itemize}
	\item 4 single-ended analog output channels
	\item Output voltage range : +/-10V (50 ohms)
	\item	16-bit DACs
	\item Up to 10MS/s
	\item 1 external clock input (single-ended)
	\item 1 start input (TTL)
	\item 1 pause input (TTL)
	\item Each channel can store up to 32 waveform.
	\item Memory/channel is determined by FMC carrier~\cite{vmefmccarier}~\cite{pciefmccarier}.
	\item Analog outputs auto-calibration.
	\item $I^2C$ EEPROM to store IPMI information and DACs calibration factors
\end{itemize}


\section{General description}
An FMC3 is a mezzanine card in FMC format, containing four identical analog output channels. Each channel has a 10MS/s 16-bit DAC with an output range of +/-10V. In addition, the card has two trigger inputs (start and pause) and one external clock input. These inputs are common to the four analog channels.

\begin{comment}
\textcolor{red}{
\subsection{Name}
Following LHC equipment code naming convention, the FMC3 card is named: 
\textbf{CFCOC} (Controls and Communication, Front End, PC front end computer, I/O module)
}
\end{comment}


\section{Clock input}
The FMC3 card has an external clock input. It is compatible with many different electrical standards, like TTL, LVTTL, PECL, sine-wave, etc...

External clock input features:
\begin{itemize}
	\item 50 ohms terminated
	\item	100Hz to 10MHz
	\item 0.6Vpp to 1Vpp (max. 10Vpp)
\end{itemize}

Nevertheless, the external clock input doesn't have to be used, as DACs clock can be provided by the FMC carrier card~\cite{vmefmccarier}~\cite{pciefmccarier}.


\section{Trigger inputs}
The FMC3 has two trigger inputs. A start and pause input. They can be used in many different ways. For further information see the \textit{FMC3 HDL functional specification}.

All trigger inputs are TTL compliant with an internal 50 ohms termination.


\section{Analog outputs}

Table ~\ref{table:analogout} lists expected FMC3 analog outputs characteristics. These are taken from a National Instruments PCI analog output card ~\cite{ni6731spec}.

\begin{table}[ht]%
\centering
\caption{Analog outputs characteristics}
\begin{tabular}{l l}
\hline
Range & $\pm$10V \\
\hline
INL\footnotemark[1] &  $\pm$2.2 LSB max. \\
\hline
DNL\footnotemark[1] &  $\pm$1.0 LSB max. \\
\hline
Monotonicity\footnotemark[1] & 16 bits \\
\hline
Offset error\footnotemark[1] & $\pm$168uV max \\
\hline
Gain error\footnotemark[1] & $\pm$30ppm of output max \\
\hline
Offset temperature coefficient & $\pm$35$\mu$V/$^{\circ}$C \\
\hline
Gain temperature coefficient & $\pm$6.5ppm/$^{\circ}$C \\
\hline
Onboard calibration reference & \\
Level & 5.000V \\
Temperature coefficient & $\pm$0.6ppm/$^{\circ}$C \\
Long-term stability & $\pm$15ppm/1000h \\
\hline
Slew rate & >15V/$\mu$s \\
\hline
Noise &  80$\mu$V, DC to 5MHz \\
\hline
Channel crosstalk & -95dB \\
\hline
Settling time & 25ns to $\pm$1 LSB accuracy \\
\hline
Total harmonic distortion & -90dB typ \\
 & (generating a 10V, 1000 points, \\
 & 7.5kHz sinewave, summing 9 harmonics) \\
\hline
\end{tabular}
\label{table:analogout}
\end{table}

\footnotetext[1]{Measured after calibration}


\section{Auto-calibration}
Analog output channels can be auto-calibrated via an on-board ADC. Calibration information is then stored in the on-board EEPROM.


\section{Physical description}

Figure ~\ref{fig:fmc_board_shape} shows the standard single width FMC board shape. The FMC3 card is connected to its carrier through a high-pin count connector from Samtec (see FMC standard~\cite{fmcstd} for further details), also visible in figure ~\ref{fig:fmc_board_shape}.

\begin{figure}[ht]
	\centering
	\includegraphics[scale=1]{fmc_board_shape.pdf}
	\caption{FMC single width board shape (scale 1:1)}
	\label{fig:fmc_board_shape}
\end{figure}


\bibliographystyle{plain}
\bibliography{fmc3_references}

\end{document}
